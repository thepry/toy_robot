# frozen_string_literal: true
# Controlls robot initialization and movements
class Robot
  DIRECTIONS = %i(NORTH EAST SOUTH WEST).freeze
  DISPLAY = { NORTH: '^', EAST: '>', SOUTH: '˅', WEST: '<' }.freeze

  attr_accessor :x, :y, :direction

  def initialize(x, y, direction)
    @x = Integer(x)
    @y = Integer(y)
    @direction = direction.strip.to_sym
  rescue ArgumentError
  end

  def valid?(max_x = 4, max_y = 4)
    check_position(max_x, max_y) && check_direction
  end

  def display
    DISPLAY[direction]
  end

  def report
    puts "#{x},#{y},#{direction}"
  end

  def left
    @direction = next_direction(-1)
  end

  def right
    @direction = next_direction
  end

  def move(field_width, field_height)
    if (0...field_width).cover?(next_x) && (0...field_height).cover?(next_y)
      @x = next_x
      @y = next_y
    end
  end

  def next_x
    case direction
    when :EAST then
      x + 1
    when :WEST then
      x - 1
    else
      x
    end
  end

  def next_y
    case direction
    when :NORTH then
      y + 1
    when :SOUTH then
      y - 1
    else
      y
    end
  end

  private

  def check_direction
    DIRECTIONS.include?(direction)
  end

  def check_position(max_x, max_y)
    check_x(max_x) && check_y(max_y)
  end

  def check_x(max)
    x && x >= 0 && x <= max
  end

  def check_y(max)
    y && y >= 0 && y <= max
  end

  def next_direction(i = 1)
    (DIRECTIONS * 2)[DIRECTIONS.index(direction) + i]
  end
end
