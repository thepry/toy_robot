# frozen_string_literal: true
require './game.rb'

game = Game.new(true)

commands = [
  'SHOW_FIELD',
  'MOVE',
  'MOVE',
  'MOVE',
  'MOVE',
  'LEFT',
  'REPORT',
  'LEFT',
  'LEFT',
  'LEFT',
  'RIGHT',
  'MOVE',
  'RIGHT',
  'MOVE',
  'MOVE',
  'MOVE',
  'REPORT',
  'PLACE 0,0,EAST',
  'MOVE',
  'RIGHT',
  'MOVE',
  'REPORT'
]

puts 'PLACE 1,2,EAST'
game.process_initial_message('PLACE 1,2,EAST')

commands.each do |cmd|
  puts
  puts cmd
  puts
  game.process_message(cmd)
  sleep(1)
end
