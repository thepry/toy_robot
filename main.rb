# frozen_string_literal: true
require './game.rb'

Game.new(ARGV.empty?, ARGV.empty?).start if __FILE__ == $PROGRAM_NAME
