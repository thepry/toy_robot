# frozen_string_literal: true
# Game field rendering and properties
class Field
  attr_accessor :state, :width, :height, :robot

  def initialize(width = 5, height = 5)
    @state = Array.new(width) { Array.new(height) }
    @width = width
    @height = height
  end

  def display
    (0...@height).reverse_each do |i|
      @state.each_with_index do |_, n|
        print " #{show_cell(n, i)} "
      end
      puts
    end
  end

  def show_cell(x, y)
    return robot.display if @robot && @robot.x == x && @robot.y == y
    '.'
  end

  def place_robot(robot)
    @robot = robot
  end
end
