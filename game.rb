# frozen_string_literal: true

require './field.rb'
require './robot.rb'

# Main class that controlls game loop, reads input and sends messages to field
# and robot
class Game
  def initialize(display_field_state = false, interactive = false)
    @display_field_state = display_field_state
    @interactive = interactive
    @field = Field.new
  end

  def start
    if @interactive
      puts 'Starting new robot toy game'
      puts
    end
    game_loop
  end

  def place_robot(message)
    args = message.split(' ').last.split(',')
    robot = Robot.new(*args) if args.size == 3
    if robot&.valid?
      @robot = robot
      @field.place_robot @robot
    else
      puts 'Incorrect attributes for PLACE command. Use "PLACE 0,0,EAST" syntax'
    end
  end

  def game_loop
    loop do
      message = gets&.strip
      return unless message
      if @robot
        process_message message
      else
        process_initial_message message
      end
    end
  end

  def process_initial_message(message)
    puts if @interactive
    case message.split(' ').first&.to_sym
    when :PLACE then
      place_robot message
    else
      puts 'Incorrect command! You should place your robot first (PLACE X,Y,F)'
    end
    puts if @interactive
  end

  def process_message(message)
    puts if @interactive
    case message.split(' ').first.to_sym
    when :PLACE then
      place_robot message
      display_field
    when :SHOW_FIELD then
      @field.display
    when :REPORT then
      @robot.report
    when :LEFT
      @robot.left
      display_field
    when :RIGHT
      @robot.right
      display_field
    when :MOVE
      @robot.move(@field.width, @field.height)
      display_field
    else
      puts 'Incorrect command! Please try again.'
    end
    puts if @interactive
  end

  def display_field
    @field.display if @display_field_state
  end
end
